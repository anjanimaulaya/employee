import React, { Component } from "react";
import "./App.css";
import employees from "../data/employees.json";
import Containers from "../Containers/Containers";
import "bootstrap/dist/css/bootstrap.min.css";
import JumboHeader from "../Containers/JumboHeader";


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      search: employees,
      employees,
      selectedEmployee: null
    };
    
  }

  handleSearch = event => {
    console.log(event.target.value)
    this.setState({
      search: this.state.employees.filter(item =>
        item.name.toLowerCase().includes(event.target.value.toLowerCase())
      )
    });
  };

  detailEmployee = (id) => {
    const index = this.state.employees.findIndex(employee => {
      return employee.id === id;
    });
    const employeeChange = { ...this.state.employees[index]};
    this.setState({selectedEmployee : employeeChange})
  };

  render() {
    return (
      <div className="App">
        <JumboHeader />
        <Containers
          handleSearch={this.handleSearch}
          employees={this.state.search}
          employee={this.state.selectedEmployee}
          detailEmployee = {this.detailEmployee}
        />
      </div>
    );
  }
}

export default App;
