import React, { Component } from "react";
import Container from "react-bootstrap/Container";
import { Row, Col, FormGroup } from "react-bootstrap";
import SearchBar from "./SearchBar";
import EmployeeList from "../Components/EmployeeList";
import Employee from "../Components/Employee";

class Containers extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <Container>
          <Row>
            <Col>
              <FormGroup>
                <h2>Employee List</h2>
              </FormGroup>
              <FormGroup>
                <SearchBar handleSearch={this.props.handleSearch} />
              </FormGroup>
              {this.props.employees.map(employees => (
                <FormGroup>
                  <EmployeeList
                    key={employees.id}
                    detailEmployee={this.props.detailEmployee}
                    idEmployee={employees.id}
                    imageEmployee={employees.photo}
                    nameEmployee={employees.name}
                    positionEmployee={employees.position}
                  />
                </FormGroup>
              ))}
            </Col>
            <Col>
              <FormGroup>
                <h2>Employee Detail</h2>
              </FormGroup>
              <FormGroup>
                <Employee employee={this.props.employee}></Employee>
              </FormGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default Containers;
