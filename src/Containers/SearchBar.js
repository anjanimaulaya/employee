import React, { Component } from "react";
import { Form,FormControl } from "react-bootstrap";

class SearchBar extends Component {
  render() {
    return (
      <div className="searchbar-container">
        <Form>
          <FormControl
            type="text"
            size="45"
            placeholder="Search Employee"
            onChange= {(event) => this.props.handleSearch(event)}
          ></FormControl>
        </Form>
      </div>
    );
  }
}

export default SearchBar;
