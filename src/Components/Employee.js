import React from "react";
import { FormGroup, Col, ListGroup, Image, Row } from "react-bootstrap";
export default function Employee(props) {
  console.log(props.employee);
  return (
    <>
      {props.employee === null ? (<h5>Click an employee</h5>
      ) : (
        <div>
          {" "}
          <Col>
            <Row>
              <FormGroup>
                <Image
                  src={props.employee.photo + "?img=" + props.employee.id}
                  rounded
                />
              </FormGroup>
            </Row>
            <Row>
              <FormGroup>
                <Row>{props.employee.name}</Row>
                <Row>{props.employee.position}</Row>
              </FormGroup>
            </Row>
            <Row>
              <FormGroup>
                {Object.values(props.employee.detail).map(item => (
                  <Row>{item}</Row>
                ))}
              </FormGroup>
            </Row>
          </Col>
        </div>
      )}
    </>
  );
}
