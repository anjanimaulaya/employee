import React, { Component } from "react";
import { FormGroup, Col, ListGroup, Image, Row } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";

class Employee extends Component {
  render() {
    return (
      <Col>
        <FormGroup onClick = {()=>this.props.detailEmployee(this.props.idEmployee)}>
          <ListGroup.Item>
            <Row>
              <Col>
                <Image
                  style={{ maxWidth: "50%" }}
                  src={this.props.imageEmployee + "?img=" + this.props.idEmployee}
                  roundedCircle
                />
              </Col>
              <Col className="text-left">
                <Row>{this.props.nameEmployee}</Row>
                <Row>{this.props.positionEmployee}</Row>
              </Col>
              <Col></Col>
            </Row>
          </ListGroup.Item>
        </FormGroup>
      </Col>
    );
  }
}

export default Employee;
